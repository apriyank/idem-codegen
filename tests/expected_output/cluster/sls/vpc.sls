{% if not params["create_vpc"]  %}
data.aws_vpc.vpc:
  exec.run:
  - path: aws.ec2.vpc.get
  - kwargs:
      tags:
        Name: {{ params["VpcName"] }}
{% endif %}

{% if not params["create_subnets"]  %}
data.aws_subnets.pvt_subnets:
  exec.run:
  - path: aws.ec2.subnet.list
  - kwargs:
      tags:
        Name: {{ params["pvt_subnet_name"] }}
{% endif %}

{% if not params["create_subnets"]  %}
data.aws_subnets.public_subnets:
  exec.run:
  - path: aws.ec2.subnet.list
  - kwargs:
      tags:
        Name: {{ params["public_subnet_name"] }}
{% endif %}

{% if params["transit-gw"] > 0  %}
data.aws_ec2_transit_gateway.region-tgw:
  exec.run:
  - path: aws.ec2.transit_gateway.get
  - kwargs:
      tags:
        Name:
        - {{ params["profile"] }}-tgw-{{ params["region"] }}
{% endif %}

{% if params["create_vpc"]  %}
aws_vpc.cluster-0:
  aws.ec2.vpc.present:
  - name: vpc-0738f2a523f4735bd
  - resource_id: {{ params.get("aws_vpc.cluster-0")}}
  - instance_tenancy: default
  - tags: {% set x = params["local_tags"].copy() %}
          {% do x.update({"Name": params["clusterName"]+"-temp-xyz-cluster-node", "kubernetes.io/cluster/"+params["clusterName"]: "shared"}) %}
          {{x}}
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-02ab4e7064a606d2b
      CidrBlock: {{ params["VpcSuperNet"] }}0.0/16
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: true
  - enable_dns_support: true
{% endif %}

{% if params["create_subnets"]  %}
{% for i in range(3) %}
aws_subnet.cluster-{{i}}:
  aws.ec2.subnet.present:
  - name: {{ params.get("aws_subnet.cluster-"+(i | string))}}
  - resource_id: {{ params.get("aws_subnet.cluster-"+(i | string))}}
  {% if params["create_vpc"]  %}
  - vpc_id:  ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  {% else %}
  - vpc_id: ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}
  {% endif %}
  {% if params["create_vpc"]  %}
  - cidr_block: {{ params["VpcSuperNet"] }}+( (i * 64) | string).0/18
  {% else %}
  - cidr_block: {{ params["cluster_pvt_subnet_cidr"][i] }}
  {% endif %}
  - availability_zone: eu-west-3a
  - tags: {% set x = params["local_tags"].copy() %}
          {% do x.update({"Name": params["clusterName"]+"-temp-xyz-node-private", "kubernetes.io/cluster/"+params["clusterName"]: "shared", "kubernetes.io/role/internal-elb": "1"}) %}
          {{x}}
{% endfor %}
{% endif %}

{% if params["create_subnets"]  %}
{% for j in range(3) %}
aws_subnet.xyz_public_subnet-{{j}}:
  aws.ec2.subnet.present:
  - name: {{ params.get("aws_subnet.xyz_public_subnet-"+(j | string))}}
  - resource_id: {{ params.get("aws_subnet.xyz_public_subnet-"+(j | string))}}
  {% if params["create_vpc"]  %}
  - vpc_id:  ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  {% else %}
  - vpc_id: ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}
  {% endif %}
  {% if params["create_vpc"]  %}
  - cidr_block: {{ params["VpcSuperNet"] }}+( (192 + j * 16) | string).0/20
  {% else %}
  - cidr_block: {{ params["cluster_public_subnet_cidr"][j] }}
  {% endif %}
  - availability_zone: eu-west-3a
  - tags: {% set x = params["local_tags"].copy() %}
          {% do x.update({"Name": params["clusterName"]+"-temp-xyz-control-public", "kubernetes.io/cluster/"+params["clusterName"]: "shared", "kubernetes.io/role/elb": "1"}) %}
          {{x}}
{% endfor %}
{% endif %}

{% if params["create_vpc"]  %}
aws_internet_gateway.cluster-0:
  aws.ec2.internet_gateway.present:
  - attachments:
    - State: available
      VpcId: ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
    name: igw-0eee9bba485b312a8
    resource_id: {{ params.get("aws_internet_gateway.cluster-0")}}
    tags: {% set x = params["local_tags"].copy() %}
          {% do x.update({"Name": params["clusterName"]+"-temp-xyz"}) %}
          {{x}}
  {% if params["create_vpc"]  %}
  - vpc_id:  ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  {% else %}
  - vpc_id: ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}
  {% endif %}
{% endif %}

{% if params["create_vpc"]  %}
{% for k in range(3) %}
aws_eip.nat_eip-{{k}}:
  aws.ec2.elastic_ip.present:
  - name: {{ params.get("aws_eip.nat_eip-"+(k | string))}}
  - resource_id: {{ params.get("aws_eip.nat_eip-"+(k | string))}}
  - domain: vpc
  - network_border_group: {{params["region"]}}
  - public_ipv4_pool: amazon
  - tags: {% set x = params["local_tags"].copy() %}
          {% do x.update({"Name": params["clusterName"]+"-natgw-eip-"+(k | string)}) %}
          {{x}}
{% endfor %}
{% endif %}

{% if params["create_vpc"]  %}
{% for l in range(3) %}
aws_nat_gateway.nat_gateway-{{l}}:
  aws.ec2.nat_gateway.present:
  - name: {{ params.get("aws_nat_gateway.nat_gateway-"+(l | string))}}
  - resource_id: {{ params.get("aws_nat_gateway.nat_gateway-"+(l | string))}}
  - subnet_id: ${aws.ec2.subnet:aws_subnet.xyz_public_subnet-{{l}}:resource_id}
  - connectivity_type: public
  - tags: {% set x = params["local_tags"].copy() %}
          {% do x.update({"Name": params["clusterName"]+"-natgw-"+(l | string)}) %}
          {{x}}
  - allocation_id: ${aws.ec2.elastic_ip:aws_eip.nat_eip-{{l}}:allocation_id}
{% endfor %}
{% endif %}

{% if params["create_vpc"]  %}
{% for m in range(3) %}
aws_route_table.cluster-{{m}}:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-080a96c3caa4d67bc
      RouteTableId: rtb-0516e0e06d933d9f4
      SubnetId: ${aws.ec2.subnet:aws_subnet.cluster-0:resource_id}
    name: {{ params.get("aws_route_table.cluster-"+(m | string))}}
    propagating_vgws: []
    resource_id: {{ params.get("aws_route_table.cluster-"+(m | string))}}
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: ${aws.ec2.nat_gateway:aws_nat_gateway.nat_gateway-{{m}}:resource_id}
      Origin: CreateRoute
      State: active
    tags: {% set x = params["local_tags"].copy() %}
          {% do x.update({"Name": params["clusterName"]+"-xyz-private-"+(m | string)}) %}
          {{x}}
  {% if params["create_vpc"]  %}
  - vpc_id:  ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  {% else %}
  - vpc_id: ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}
  {% endif %}
{% endfor %}
{% endif %}

{% if params["create_vpc"]  %}
{% for n in range(3) %}
aws_route_table.xyz_public-{{n}}:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0f2b91e5f78d7af47
      RouteTableId: rtb-01e542a8c56c9511f
      SubnetId: ${aws.ec2.subnet:aws_subnet.xyz_public_subnet-0:resource_id}
    name: {{ params.get("aws_route_table.xyz_public-"+(n | string))}}
    propagating_vgws: []
    resource_id: {{ params.get("aws_route_table.xyz_public-"+(n | string))}}
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: ${aws.ec2.internet_gateway:aws_internet_gateway.cluster-0:resource_id}
      Origin: CreateRoute
      State: active
    tags: {% set x = params["local_tags"].copy() %}
          {% do x.update({"Name": params["clusterName"]+"-xyz-public-"+(n | string)}) %}
          {{x}}
  {% if params["create_vpc"]  %}
  - vpc_id:  ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  {% else %}
  - vpc_id: ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}
  {% endif %}
{% endfor %}
{% endif %}

{% if params["create_vpc"]  %}
aws_flow_log.redlock_flow_log:
  aws.ec2.flow_log.present:
  - resource_ids:
    - ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  - resource_type: VPC
  - resource_id: {{ params.get("aws_flow_log.redlock_flow_log")}}
  - iam_role: ${aws.iam.role:aws_iam_role.redlock_flow_role:arn}
  - log_group_name: ${aws.cloudwatch.log_group:aws_cloudwatch_log_group.redlock_flow_log_group:resource_id}
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []
{% endif %}

{% if params["create_vpc"]  %}
aws_cloudwatch_log_group.redlock_flow_log_group:
  aws.cloudwatch.log_group.present:
  - name: xyz-{{ params["clusterName"] }}_redlock_flow_log_group
  - resource_id: {{ params.get("aws_cloudwatch_log_group.redlock_flow_log_group")}}
  - tags: {{ params["local_tags"] }}
{% endif %}

{% if params["create_vpc"]  %}
aws_iam_role.redlock_flow_role:
  aws.iam.role.present:
  - resource_id: {{ params.get("aws_iam_role.redlock_flow_role")}}
  - name: xyz-{{ params["clusterName"] }}_redlock_flow_role
  - max_session_duration: 3600
  - tags: {{ params["local_tags"] }}
  - assume_role_policy_document:
      {
          "Statement": [
              {
                  "Action": "sts:AssumeRole",
                  "Effect": "Allow",
                  "Principal": {
                      "Service": "vpc-flow-logs.amazonaws.com"
                  },
                  "Sid": ""
              }
          ],
          "Version": "2012-10-17"
      }
{% endif %}

{% if params["create_vpc"]  %}
aws_iam_role_policy.redlock_flow_policy:
  aws.iam.role_policy.present:
  - resource_id: {{ params.get("aws_iam_role_policy.redlock_flow_policy")}}
  - role_name: ${aws.iam.role:aws_iam_role.redlock_flow_role:resource_id}
  - name: xyz-{{ params["clusterName"] }}_redlock_flow_policy
  - policy_document:
      {
          "Statement": [
              {
                  "Action": [
                      "logs:CreateLogGroup",
                      "logs:CreateLogStream",
                      "logs:PutLogEvents",
                      "logs:DescribeLogGroups",
                      "logs:DescribeLogStreams"
                  ],
                  "Effect": "Allow",
                  "Resource": "*"
              }
          ],
          "Version": "2012-10-17"
      }
{% endif %}

{% if params["create_vpc"]  %}
aws_vpc_dhcp_options.vpc_options:
  aws.ec2.dhcp_option.present:
  - name: dopt-052512c363a0a800b
  - resource_id: {{ params.get("aws_vpc_dhcp_options.vpc_options")}}
  - tags: {% set x = params["local_tags"].copy() %}
          {% do x.update({"Name": "xyz-"+params["clusterName"]}) %}
          {{x}}
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - us-west-2.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS
{% endif %}

{% if params["create_vpc"]  %}
aws_db_subnet_group.db-subnet-group:
  aws.rds.db_subnet_group.present:
  - db_subnet_group_arn: arn:aws:rds:eu-west-3:123456789012:subgrp:db-subnet-group-idem-test
    db_subnet_group_description: For Aurora rds
    name: db-subnet-group-{{ params["clusterName"] }}
    resource_id: {{ params.get("aws_db_subnet_group.db-subnet-group")}}
  {% if params["create_subnets"]  %}
  - subnets:  ${aws.ec2.subnet:aws_subnet.cluster:[*]:resource_id}
  {% else %}
  - subnets: ${aws.ec2.subnet:data.aws_subnets.pvt_subnets:[*]:resource_id}
  {% endif %}
    tags: {% set x = params["local_tags"].copy() %}
          {% do x.update({"Name": params["clusterName"]+"-db-subnet-group"}) %}
          {{x}}
{% endif %}

{% if params["create_vpc"]  %}
aws_elasticache_subnet_group.default:
  aws.elasticache.cache_subnet_group.present:
  - name: elasticache-subnet-group-{{ params["clusterName"] }}
  - resource_id: {{ params.get("aws_elasticache_subnet_group.default")}}
  - cache_subnet_group_description: For elastcache redis cluster
  {% if params["create_subnets"]  %}
  - subnet_ids:  ${aws.ec2.subnet:aws_subnet.cluster:[*]:resource_id}
  {% else %}
  - subnet_ids: ${aws.ec2.subnet:data.aws_subnets.pvt_subnets:[*]:resource_id}
  {% endif %}
  - tags: []
{% endif %}
