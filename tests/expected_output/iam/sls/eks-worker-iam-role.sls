data.aws_vpc.vpc:
  exec.run:
  - path: aws.ec2.vpc.get
  - kwargs:
      tags:
        Name: {{ params["VpcName"] }}

data.aws_subnets.pvt_subnets:
  exec.run:
  - path: aws.ec2.subnet.list
  - kwargs:
      tags:
        Name: {{ params["pvt_subnet_name"] }}

aws_iam_role.cluster-node:
  aws.iam.role.present:
  - resource_id: {{ params.get("aws_iam_role.cluster-node")}}
  - name: {{ params["clusterName"] }}-temp-xyz-cluster-node
  - max_session_duration: 3600
  - tags: {{ params["local_tags"] }}
  - assume_role_policy_document:
      {
          "Statement": [
              {
                  "Action": "sts:AssumeRole",
                  "Effect": "Allow",
                  "Principal": {
                      "Service": "ec2.amazonaws.com"
                  }
              }
          ],
          "Version": "2012-10-17"
      }

aws_iam_role_policy_attachment.cluster-node-AmazonxyzWorkerNodePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: ${aws.iam.role:aws_iam_role.cluster-node:resource_id}
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzWorkerNodePolicy

aws_iam_role_policy_attachment.cluster-node-Amazonxyz_CNI_Policy:
  aws.iam.role_policy_attachment.present:
  - role_name: ${aws.iam.role:aws_iam_role.cluster-node:resource_id}
  - policy_arn: arn:aws:iam::aws:policy/Amazonxyz_CNI_Policy

aws_iam_role_policy_attachment.cluster-node-AmazonEC2ContainerRegistryReadOnly:
  aws.iam.role_policy_attachment.present:
  - role_name: ${aws.iam.role:aws_iam_role.cluster-node:resource_id}
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly

aws_iam_role_policy_attachment.cluster-node-AmazonEC2RoleforSSM:
  aws.iam.role_policy_attachment.present:
  - role_name: ${aws.iam.role:aws_iam_role.cluster-node:resource_id}
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM

aws_iam_role_policy_attachment.cluster-node-AmazonElasticFileSystemFullAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: ${aws.iam.role:aws_iam_role.cluster-node:resource_id}
  - policy_arn: arn:aws:iam::aws:policy/AmazonElasticFileSystemFullAccess

aws_iam_role_policy_attachment.cluster-node-AmazonEC2ReadOnlyAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: ${aws.iam.role:aws_iam_role.cluster-node:resource_id}
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess

aws_iam_role_policy.credstash_access_policy:
  aws.iam.role_policy.present:
  - resource_id: {{ params.get("aws_iam_role_policy.credstash_access_policy")}}
  - role_name: ${aws.iam.role:aws_iam_role.cluster-node:resource_id}
  - name: credstash_xyz_{{ params["clusterName"] }}_access_policy
  - policy_document:
      {
          "Statement": [
              {
                  "Action": [
                      "kms:GenerateDataKey",
                      "kms:Decrypt"
                  ],
                  "Effect": "Allow",
                  "Resource": "arn:aws:kms:eu-west-3:123456789012:key/8ac5f341-fd1c-4e9d-9596-8f844dba5cc8"
              },
              {
                  "Action": [
                      "dynamodb:PutItem",
                      "dynamodb:GetItem",
                      "dynamodb:Query",
                      "dynamodb:Scan"
                  ],
                  "Effect": "Allow",
                  "Resource": "arn:aws:dynamodb:{{ params[\"region\"] }}:params[\"account_id\"]:table/xyz-{{ params[\"clusterName\"] }}-credential-store"
              }
          ],
          "Version": "2012-10-17"
      }

aws_iam_role_policy.Amazon_EBS_CSI_Driver:
  aws.iam.role_policy.present:
  - resource_id: {{ params.get("aws_iam_role_policy.Amazon_EBS_CSI_Driver")}}
  - role_name: ${aws.iam.role:aws_iam_role.cluster-node:resource_id}
  - name: Amazon_EBS_CSI_Driver
  - policy_document:
      {
          "Statement": [
              {
                  "Action": [
                      "ec2:AttachVolume",
                      "ec2:CreateSnapshot",
                      "ec2:CreateTags",
                      "ec2:CreateVolume",
                      "ec2:DeleteSnapshot",
                      "ec2:DeleteTags",
                      "ec2:DeleteVolume",
                      "ec2:DescribeAvailabilityZones",
                      "ec2:DescribeInstances",
                      "ec2:DescribeSnapshots",
                      "ec2:DescribeTags",
                      "ec2:DescribeVolumes",
                      "ec2:DescribeVolumesModifications",
                      "ec2:DetachVolume",
                      "ec2:ModifyVolume"
                  ],
                  "Effect": "Allow",
                  "Resource": "*"
              }
          ],
          "Version": "2012-10-17"
      }
