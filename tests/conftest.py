import pathlib
import unittest.mock as mock

import dict_tools
import pytest

test_dir = pathlib.Path(__file__).parent.resolve()

test_config_map = {
    "idem_codegen": {
        "get_tf_state_from_s3": False,
        "output_directory_path": f"{test_dir}/output",
        "idem_describe_path": f"{test_dir}/resources/idem_describe_response.sls",
        "tf_state_file_path": f"{test_dir}/resources/tfstate.json",
        "terraform_directory_path": f"{test_dir}/resources/input",
        "run_name": "tf_idem",
        "group_style": "default",
        "private_token": "glpat-SF85s-1PT8_9z3ursP7m",
        "gitlab_link": "https://gitlab.com/",
        "project_branch": "master",
    }
}


@pytest.fixture()
def hub(hub, test_config):
    hub.pop.sub.add(dyne_name="idem_codegen")
    hub.test = dict_tools.data.NamespaceDict(
        idem_codegen=dict(
            current_path=test_dir,
            unit_test=True,
            persist_output=False,
            tfstate_v3=False,
        )
    )
    with mock.patch("sys.argv", ["state", "thing"]):
        hub.pop.config.load(["idem_codegen"], cli="idem_codegen", parse_cli=False)

    hub.OPT = hub.pop.data.imap(test_config_map)
    yield hub


def pytest_addoption(parser):
    parser.addoption(
        "--test_config", action="store", default="", help="test_config value"
    )


@pytest.fixture
def test_config(request):
    yield request.config.getoption("--test_config")


@pytest.fixture
def change_test_dir(monkeypatch):
    current_path = pathlib.Path(__file__).parent.resolve()
    monkeypatch.chdir(current_path / "resources" / "input")
