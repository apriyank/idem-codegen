import pytest

from tests.test_utils import compare_folders_for_equality


@pytest.mark.asyncio
async def test_tf_idem_e2e_run(hub, change_test_dir):
    hub.test.idem_codegen.unit_test = False
    hub.test.idem_codegen.run_name = "tf_idem"
    await hub.idem_codegen.init.run()
    output_dir_path = f"{hub.test.idem_codegen.current_path}/output"
    expected_output_dir_path = f"{hub.test.idem_codegen.current_path}/expected_output"
    compare_folders_for_equality(hub, output_dir_path, expected_output_dir_path)
