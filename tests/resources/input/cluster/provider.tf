provider "aws" {
  region  = var.region
  profile = var.profile
}

provider "aws" {
  alias   = "assumed"
  profile = var.profile
  region  = var.region
  assume_role {
    role_arn     = var.aws_iam_role-xyz-admin
    session_name = "xyz-cluster-creation"
  }
}

data "aws_caller_identity" "current" {
}

data "aws_availability_zones" "available" {
}

provider "credstash" {
  alias   = "cluster_local_table"
  profile = var.profile
  table   = "xyz-${var.clusterName}-credential-store"
  region  = var.region
}

provider "aws" {
  alias   = "cross_account_beachops"
  region  = var.region
  profile = var.cross_account_beachops_domain_profile != "" ? var.cross_account_beachops_domain_profile : var.profile
}
